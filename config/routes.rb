Rails.application.routes.draw do

  use_doorkeeper
  get 'welcome/index'

  resources :cost_centers
  resources :movement_states
  resources :movement_types
  resources :categories
  resources :account_movements
  resources :companies
  devise_for :users

  authenticated :user do
    devise_scope :user do
      root to: "dashboard#index"
    end
  end

  unauthenticated do
    devise_scope :user do
      root to: "welcome#index", :as => "unauthenticated"
    end
  end

  
  use_doorkeeper # doorkeeper
  mount API::Root => '/'
  #mount API::Base => '/api' # Grape

resources :documentation, only: [:index] do # swagger-ui
  collection do # documentation token redirect
    get :o2c
    get :authorize
  end
end


end
