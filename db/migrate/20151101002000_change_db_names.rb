class ChangeDbNames < ActiveRecord::Migration
  def change
    remove_column :account_movements, :state_id
    remove_column :account_movements, :type_id
    add_column :account_movements, :movement_state_id, :integer
    add_column :account_movements, :movement_type_id, :integer
  end
end
