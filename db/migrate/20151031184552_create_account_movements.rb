class CreateAccountMovements < ActiveRecord::Migration
  def change
    create_table :account_movements do |t|
      t.string :synopsis
      t.text :description
      t.boolean :income_output
      t.decimal :amount
      t.integer :state_id
      t.integer :category_id
      t.integer :type_id
      t.date :date
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
