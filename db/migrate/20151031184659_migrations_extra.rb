class MigrationsExtra < ActiveRecord::Migration
  def change
    add_column :account_movements, :cost_center_id, :integer
    add_attachment :account_movements, :requisition
    add_attachment :account_movements, :receipt

    add_index :companies, :user_id
    add_index :account_movements, :state_id
    add_index :account_movements, :category_id
    add_index :account_movements, :type_id
    add_index :account_movements, :company_id
    add_index :account_movements, :cost_center_id


    add_index :categories, :company_id
    add_index :cost_centers, :company_id
    add_index :movement_states, :company_id
    add_index :movement_types, :company_id
  end
end