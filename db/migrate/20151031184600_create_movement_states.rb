class CreateMovementStates < ActiveRecord::Migration
  def change
    create_table :movement_states do |t|
      t.string :name
      t.integer :company_id

      t.timestamps null: false
    end
  end
end
