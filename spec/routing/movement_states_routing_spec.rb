require "rails_helper"

RSpec.describe MovementStatesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/movement_states").to route_to("movement_states#index")
    end

    it "routes to #new" do
      expect(:get => "/movement_states/new").to route_to("movement_states#new")
    end

    it "routes to #show" do
      expect(:get => "/movement_states/1").to route_to("movement_states#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/movement_states/1/edit").to route_to("movement_states#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/movement_states").to route_to("movement_states#create")
    end

    it "routes to #update" do
      expect(:put => "/movement_states/1").to route_to("movement_states#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/movement_states/1").to route_to("movement_states#destroy", :id => "1")
    end

  end
end
