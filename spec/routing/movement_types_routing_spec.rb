require "rails_helper"

RSpec.describe MovementTypesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/movement_types").to route_to("movement_types#index")
    end

    it "routes to #new" do
      expect(:get => "/movement_types/new").to route_to("movement_types#new")
    end

    it "routes to #show" do
      expect(:get => "/movement_types/1").to route_to("movement_types#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/movement_types/1/edit").to route_to("movement_types#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/movement_types").to route_to("movement_types#create")
    end

    it "routes to #update" do
      expect(:put => "/movement_types/1").to route_to("movement_types#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/movement_types/1").to route_to("movement_types#destroy", :id => "1")
    end

  end
end
