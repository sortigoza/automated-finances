# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :movement_type do
    name "MyString"
    company_id 1
  end
end
