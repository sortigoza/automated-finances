# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :account_movement do
    synopsis "MyString"
    description "MyText"
    income_output false
    amount "9.99"
    state_id 1
    category_id 1
    type_id 1
    date "2015-10-31"
    company_id 1
  end
end
