require 'rails_helper'

RSpec.describe "cost_centers/edit", :type => :view do
  before(:each) do
    @cost_center = assign(:cost_center, CostCenter.create!(
      :name => "MyString",
      :company_id => 1
    ))
  end

  it "renders the edit cost_center form" do
    render

    assert_select "form[action=?][method=?]", cost_center_path(@cost_center), "post" do

      assert_select "input#cost_center_name[name=?]", "cost_center[name]"

      assert_select "input#cost_center_company_id[name=?]", "cost_center[company_id]"
    end
  end
end
