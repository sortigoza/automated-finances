require 'rails_helper'

RSpec.describe "cost_centers/new", :type => :view do
  before(:each) do
    assign(:cost_center, CostCenter.new(
      :name => "MyString",
      :company_id => 1
    ))
  end

  it "renders new cost_center form" do
    render

    assert_select "form[action=?][method=?]", cost_centers_path, "post" do

      assert_select "input#cost_center_name[name=?]", "cost_center[name]"

      assert_select "input#cost_center_company_id[name=?]", "cost_center[company_id]"
    end
  end
end
