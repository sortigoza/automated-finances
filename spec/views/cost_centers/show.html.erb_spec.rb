require 'rails_helper'

RSpec.describe "cost_centers/show", :type => :view do
  before(:each) do
    @cost_center = assign(:cost_center, CostCenter.create!(
      :name => "Name",
      :company_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/1/)
  end
end
