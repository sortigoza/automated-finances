require 'rails_helper'

RSpec.describe "cost_centers/index", :type => :view do
  before(:each) do
    assign(:cost_centers, [
      CostCenter.create!(
        :name => "Name",
        :company_id => 1
      ),
      CostCenter.create!(
        :name => "Name",
        :company_id => 1
      )
    ])
  end

  it "renders a list of cost_centers" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
