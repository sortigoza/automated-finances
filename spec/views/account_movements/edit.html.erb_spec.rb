require 'rails_helper'

RSpec.describe "account_movements/edit", :type => :view do
  before(:each) do
    @account_movement = assign(:account_movement, AccountMovement.create!(
      :synopsis => "MyString",
      :description => "MyText",
      :income_output => false,
      :amount => "9.99",
      :state_id => 1,
      :category_id => 1,
      :type_id => 1,
      :company_id => 1
    ))
  end

  it "renders the edit account_movement form" do
    render

    assert_select "form[action=?][method=?]", account_movement_path(@account_movement), "post" do

      assert_select "input#account_movement_synopsis[name=?]", "account_movement[synopsis]"

      assert_select "textarea#account_movement_description[name=?]", "account_movement[description]"

      assert_select "input#account_movement_income_output[name=?]", "account_movement[income_output]"

      assert_select "input#account_movement_amount[name=?]", "account_movement[amount]"

      assert_select "input#account_movement_state_id[name=?]", "account_movement[state_id]"

      assert_select "input#account_movement_category_id[name=?]", "account_movement[category_id]"

      assert_select "input#account_movement_type_id[name=?]", "account_movement[type_id]"

      assert_select "input#account_movement_company_id[name=?]", "account_movement[company_id]"
    end
  end
end
