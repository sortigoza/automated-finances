require 'rails_helper'

RSpec.describe "account_movements/show", :type => :view do
  before(:each) do
    @account_movement = assign(:account_movement, AccountMovement.create!(
      :synopsis => "Synopsis",
      :description => "MyText",
      :income_output => false,
      :amount => "9.99",
      :state_id => 1,
      :category_id => 2,
      :type_id => 3,
      :company_id => 4
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Synopsis/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/9.99/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
  end
end
