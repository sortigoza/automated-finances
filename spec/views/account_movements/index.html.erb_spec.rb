require 'rails_helper'

RSpec.describe "account_movements/index", :type => :view do
  before(:each) do
    assign(:account_movements, [
      AccountMovement.create!(
        :synopsis => "Synopsis",
        :description => "MyText",
        :income_output => false,
        :amount => "9.99",
        :state_id => 1,
        :category_id => 2,
        :type_id => 3,
        :company_id => 4
      ),
      AccountMovement.create!(
        :synopsis => "Synopsis",
        :description => "MyText",
        :income_output => false,
        :amount => "9.99",
        :state_id => 1,
        :category_id => 2,
        :type_id => 3,
        :company_id => 4
      )
    ])
  end

  it "renders a list of account_movements" do
    render
    assert_select "tr>td", :text => "Synopsis".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => "9.99".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
  end
end
