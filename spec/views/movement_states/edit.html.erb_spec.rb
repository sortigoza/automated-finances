require 'rails_helper'

RSpec.describe "movement_states/edit", :type => :view do
  before(:each) do
    @movement_state = assign(:movement_state, MovementState.create!(
      :name => "MyString",
      :company_id => 1
    ))
  end

  it "renders the edit movement_state form" do
    render

    assert_select "form[action=?][method=?]", movement_state_path(@movement_state), "post" do

      assert_select "input#movement_state_name[name=?]", "movement_state[name]"

      assert_select "input#movement_state_company_id[name=?]", "movement_state[company_id]"
    end
  end
end
