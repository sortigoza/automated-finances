require 'rails_helper'

RSpec.describe "movement_states/show", :type => :view do
  before(:each) do
    @movement_state = assign(:movement_state, MovementState.create!(
      :name => "Name",
      :company_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/1/)
  end
end
