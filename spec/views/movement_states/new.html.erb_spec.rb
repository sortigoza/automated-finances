require 'rails_helper'

RSpec.describe "movement_states/new", :type => :view do
  before(:each) do
    assign(:movement_state, MovementState.new(
      :name => "MyString",
      :company_id => 1
    ))
  end

  it "renders new movement_state form" do
    render

    assert_select "form[action=?][method=?]", movement_states_path, "post" do

      assert_select "input#movement_state_name[name=?]", "movement_state[name]"

      assert_select "input#movement_state_company_id[name=?]", "movement_state[company_id]"
    end
  end
end
