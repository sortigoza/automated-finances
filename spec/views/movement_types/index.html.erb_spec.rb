require 'rails_helper'

RSpec.describe "movement_types/index", :type => :view do
  before(:each) do
    assign(:movement_types, [
      MovementType.create!(
        :name => "Name",
        :company_id => 1
      ),
      MovementType.create!(
        :name => "Name",
        :company_id => 1
      )
    ])
  end

  it "renders a list of movement_types" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
