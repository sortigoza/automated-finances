require 'rails_helper'

RSpec.describe "movement_types/edit", :type => :view do
  before(:each) do
    @movement_type = assign(:movement_type, MovementType.create!(
      :name => "MyString",
      :company_id => 1
    ))
  end

  it "renders the edit movement_type form" do
    render

    assert_select "form[action=?][method=?]", movement_type_path(@movement_type), "post" do

      assert_select "input#movement_type_name[name=?]", "movement_type[name]"

      assert_select "input#movement_type_company_id[name=?]", "movement_type[company_id]"
    end
  end
end
