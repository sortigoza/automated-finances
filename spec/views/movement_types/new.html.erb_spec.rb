require 'rails_helper'

RSpec.describe "movement_types/new", :type => :view do
  before(:each) do
    assign(:movement_type, MovementType.new(
      :name => "MyString",
      :company_id => 1
    ))
  end

  it "renders new movement_type form" do
    render

    assert_select "form[action=?][method=?]", movement_types_path, "post" do

      assert_select "input#movement_type_name[name=?]", "movement_type[name]"

      assert_select "input#movement_type_company_id[name=?]", "movement_type[company_id]"
    end
  end
end
