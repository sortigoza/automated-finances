require 'rails_helper'

RSpec.describe "movement_types/show", :type => :view do
  before(:each) do
    @movement_type = assign(:movement_type, MovementType.create!(
      :name => "Name",
      :company_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/1/)
  end
end
