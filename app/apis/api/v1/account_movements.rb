# api/v1/account_movements.rb
module API
  module V1
    class AccountMovements < Grape::API
      version 'v1'
      format :json

      resource :account_movements do
        desc "Return list of recent account_movements"
        get do
          AccountMovement.all
        end
      end
    end
  end
end