class Dispatch < Grape::API
    mount API2::V1::Root
    format :json
    route :any, '*path' do
      Rack::Response.new({message: "Not found"}.to_json, 404).finish
    end
  end

  Base = Rack::Builder.new do
    use API2::Logger
    #run API2::Dispatch
  end
