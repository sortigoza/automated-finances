class MovementState < ActiveRecord::Base
  belongs_to :company
  has_many :account_movements

  def self.company_movement_states company
    # Returns a Hash
    movement_states = company.movement_states
    movement_states.map do |a|
      inputs = AccountMovement.element_incomes(a)
      outputs = AccountMovement.element_outputs(a)
      summary_in = SummaryGen.new(inputs)
      summary_out = SummaryGen.new(outputs)

      total_in_out = ((t = summary_in.total) ? t : 0) - ((t = summary_out.total) ? t : 0)
      {id: a.id, name: a.name, total_items: (inputs.size + outputs.size), 
        total_in_out: total_in_out}
    end
  end
end
