class Category < ActiveRecord::Base
  belongs_to :company
  has_many :account_movements

  def self.company_categories company
    # Returns a Hash
    categories = company.categories
    categories.map do |a|
      inputs = AccountMovement.element_incomes(a)
      outputs = AccountMovement.element_outputs(a)
      summary_in = SummaryGen.new(inputs)
      summary_out = SummaryGen.new(outputs)

      {id: a.id, name: a.name, total_items: (inputs.size + outputs.size), 
        total_in: summary_in.total, total_out: summary_out.total}
    end
  end
end
