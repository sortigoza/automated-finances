class Company < ActiveRecord::Base
  belongs_to :user
  has_many :account_movements
  has_many :cost_centers
  has_many :categories
  has_many :movement_states
  has_many :movement_types

  def self.company_users company
    # Returns a Hash
    users = [company.user]
    users.map do |a|
      {id: a.id, email: a.email}
    end
  end
end
