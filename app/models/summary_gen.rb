require 'statsample'

class SummaryGen

  def initialize collection
    preprocess_amount_data collection
  end

  def total 
    @data.sum 
  end

  def mean 
    @data.mean 
  end

  def mode 
    @data.mode 
  end

  def median 
    @data.median 
  end


  private

  def preprocess_amount_data collection
    @data = []
    if collection
      @data = collection.map { |e| e.amount } 
    end
  end


end
