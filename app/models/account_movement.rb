class AccountMovement < ActiveRecord::Base
  belongs_to :category
  belongs_to :company
  belongs_to :cost_center
  belongs_to :movement_type
  belongs_to :movement_state

  # Validations
  validates :synopsis, :amount, :date, presence: true

  def self.element_incomes element
    element.account_movements.where(income_output: true)
  end

  def self.element_outputs element
    element.account_movements.where(income_output: false)
  end

end
