class ApplicationController < ActionController::Base
  before_action :set_current_company
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def set_current_company
    return unless current_user
    @company = current_user.company
    redirect_to new_company_path, notice: 'Create a company first.' unless @company
  end
end
