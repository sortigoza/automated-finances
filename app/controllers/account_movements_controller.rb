class AccountMovementsController < ApplicationController
  before_action :set_account_movement, only: [:show, :edit, :update, :destroy]

  # GET /account_movements
  # GET /account_movements.json
  def index
    @account_incomes = AccountMovement.element_incomes(@company)
    @account_outputs = AccountMovement.element_outputs(@company)
    @summary_incomes = SummaryGen.new(@account_incomes)
    @summary_outcomes = SummaryGen.new(@account_outputs)
  end

  # GET /account_movements/1
  # GET /account_movements/1.json
  def show
  end

  # GET /account_movements/new
  def new
    @account_movement = @company.account_movements.new(income_output: params[:income_output])
    form_fields
  end

  # GET /account_movements/1/edit
  def edit
    form_fields
  end

  # POST /account_movements
  # POST /account_movements.json
  def create
    @account_movement = @company.account_movements.new(account_movement_params)

    respond_to do |format|
      if @account_movement.save
        format.html { redirect_to @account_movement, notice: 'Account movement was successfully created.' }
        format.json { render :show, status: :created, location: @account_movement }
      else
        form_fields
        format.html { render :new }
        format.json { render json: @account_movement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /account_movements/1
  # PATCH/PUT /account_movements/1.json
  def update
    respond_to do |format|
      if @account_movement.update(account_movement_params)
        format.html { redirect_to @account_movement, notice: 'Account movement was successfully updated.' }
        format.json { render :show, status: :ok, location: @account_movement }
      else
        format.html { render :edit }
        format.json { render json: @account_movement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /account_movements/1
  # DELETE /account_movements/1.json
  def destroy
    @account_movement.destroy
    respond_to do |format|
      format.html { redirect_to account_movements_url, notice: 'Account movement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def form_fields
      @categories = @company.categories
      @cost_centers = @company.cost_centers
      @types = @company.movement_types
      @states = @company.movement_states
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_account_movement
      @account_movement = AccountMovement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_movement_params
      params.require(:account_movement).permit(:synopsis, :description, :income_output, :amount, :movement_state_id, :category_id, :movement_type_id, :date, :company_id, :cost_center_id)
    end
end
