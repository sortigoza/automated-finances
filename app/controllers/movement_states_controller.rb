class MovementStatesController < ApplicationController
  before_action :set_movement_state, only: [:show, :edit, :update, :destroy]

  # GET /movement_states
  # GET /movement_states.json
  def index
    @movement_states = MovementState.company_movement_states(@company)
  end

  # GET /movement_states/1
  # GET /movement_states/1.json
  def show
  end

  # GET /movement_states/new
  def new
    @movement_state = @company.movement_states.new
  end

  # GET /movement_states/1/edit
  def edit
  end

  # POST /movement_states
  # POST /movement_states.json
  def create
    @movement_state = @company.movement_states.new(movement_state_params)

    respond_to do |format|
      if @movement_state.save
        format.html { redirect_to @movement_state, notice: 'Movement state was successfully created.' }
        format.json { render :show, status: :created, location: @movement_state }
      else
        format.html { render :new }
        format.json { render json: @movement_state.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /movement_states/1
  # PATCH/PUT /movement_states/1.json
  def update
    respond_to do |format|
      if @movement_state.update(movement_state_params)
        format.html { redirect_to @movement_state, notice: 'Movement state was successfully updated.' }
        format.json { render :show, status: :ok, location: @movement_state }
      else
        format.html { render :edit }
        format.json { render json: @movement_state.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /movement_states/1
  # DELETE /movement_states/1.json
  def destroy
    @movement_state.destroy
    respond_to do |format|
      format.html { redirect_to movement_states_url, notice: 'Movement state was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_movement_state
      @movement_state = MovementState.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def movement_state_params
      params.require(:movement_state).permit(:name, :company_id)
    end
end
