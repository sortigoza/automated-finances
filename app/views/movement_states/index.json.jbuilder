json.array!(@movement_states) do |movement_state|
  json.extract! movement_state, :id, :name, :company_id
  json.url movement_state_url(movement_state, format: :json)
end
