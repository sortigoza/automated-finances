json.array!(@account_movements) do |account_movement|
  json.extract! account_movement, :id, :synopsis, :description, :income_output, :amount, :state_id, :category_id, :type_id, :date, :company_id
  json.url account_movement_url(account_movement, format: :json)
end
